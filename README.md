![screenshot](/screenshot.png)

Black night theme

With optimized space.

With removed help button at bottom-left


For use with [Stylus](https://github.com/openstyles/stylus) for [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) or [Firefox](https://addons.mozilla.org/firefox/addon/styl-us).

Install theme from https://userstyles.world/style/14772/www-evernote-comclientweb-feb-2024
